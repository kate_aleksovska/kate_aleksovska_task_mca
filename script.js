let getData = document.getElementById("getData")
let printData = document.getElementById("printData");
let receiptDomestic = document.getElementById("receiptDomestic")
let receiptImported = document.getElementById("receiptImported")

let arrayProduct = []

let final = ''

function stringTruncate(string) {
    return string.length > 10 ? string.slice(0, 10)+'...' : string
}

function ifHasWeight(weight) {
    if(weight) {
        return weight
    } else {
        return `N/A`
    }
}
function sortObjectsByProp(objectsArr, prop, ascending = true) {
    let objectsHaveProp = objectsArr.every(object => object.hasOwnProperty(prop));
    if(objectsHaveProp)    {
        let newObjectsArr = objectsArr.slice();
        newObjectsArr.sort((a, b) => {
                let textA = a[prop].toUpperCase(),
                    textB = b[prop].toUpperCase();
                if(ascending)   {
                    return textA < textB ? -1 : textA > textB ? 1 : 0;
                } else {
                    return textB < textA ? -1 : textB > textA ? 1 : 0;
                }
        });
        return newObjectsArr;
    }
    return objectsArr;
}


getData.addEventListener('click', function(e){

    e.preventDefault()
    let sumDomestic =0
    let sumImported = 0
    let counterDomestic = 0
    let counterImported = 0
    fetch('https://interview-task-api.mca.dev/qr-scanner-codes/alpha-qr-gFpwhsQ8fkY1')
    .then(response => {
        return response.json()
    }).then(data => {
        let sortedAlfaveticly = sortObjectsByProp(data, "name")
        console.log(sortedAlfaveticly)
        console.log(data)
        let msg1 = `<div class="card text-white bg-success mb-3 animate__animated animate__bounceInUp d-inline-block mx-1" style="width: 18rem;">
        <div class="card-header">.Domestic:</div>`
        let msg2 = `<div class="card text-white bg-danger mb-3 animate__animated animate__bounceInUp d-inline-block mx-1" style="width: 18rem;">
        <div class="card-header">.Imported:</div>`
        sortedAlfaveticly.forEach(product => {
            
            if(product.domestic) {
                msg1 += 
                `<div class="card-body">
                  <h5 class="card-title">...${product.name}</h5>
                  <h5 class="card-title">Price: $${product.price}</h5>
                  <p class='card-text'>`+stringTruncate(product.description)+`</p>
                  <p class="card-text">Weight:`+ifHasWeight(product.weight)+`</p>    
                </div>`
                counterDomestic++
                sumDomestic += parseInt(product.price)
            } 
            if(!product.domestic) {
                msg2 +=`<div class="card-body">
                <h5 class="card-title">...${product.name}</h5>
                <h5 class="card-title">Price: $${product.price}</h5>
                <p class='card-text'>`+stringTruncate(product.description)+`</p>
                <p class="card-text">Weight:`+ifHasWeight(product.weight)+`</p>    
              </div>`
              counterImported++
              sumImported += parseInt(product.price)
            }
        });
        final = `<div class="card text-white d-inline-block bg-warning mb-3 animate__animated animate__bounceInUp mx-1" style="width: 18rem;">
        <div class="card-header">Final cost</div>
        <div class="card-body">
                <h5 class="card-title">Domestic cost: $${sumDomestic}</h5>
                <h5 class="card-title">Imported cost: $${sumImported}</h5>
                <p class='card-text'>Domestic count: ${counterDomestic}</p>
                <p class="card-text">Imported count: ${counterImported}</p>    
              </div>`
        msg1 +=`</div>`
        msg2 +=`</div>`
        receiptDomestic.innerHTML = msg1 + msg2 + final
    })
    .catch(error => console.log("Error:", error))
})
